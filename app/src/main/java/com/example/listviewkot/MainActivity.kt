package com.example.listviewkot

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    //SE DECLARAN LAS VARIABLES A UTILIZAR
    private lateinit var LvPaises: ListView;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //RELACION DE VARIABLES DECLARADAS CON LAS ID DE XML
        LvPaises = findViewById<View>(R.id.LvPaisesxml) as ListView
        val Adaptador = ArrayAdapter(
            this@MainActivity,
            android.R.layout.simple_expandable_list_item_1,
            resources.getStringArray(R.array.paises)
        )
        LvPaises.setAdapter(Adaptador)

        //hacer esto
        LvPaises.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                Toast.makeText(
                    this@MainActivity,
                    "Selecciono el pais " + adapterView.getItemAtPosition(i).toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        })
    }
}